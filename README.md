ansible-kerberos-server
=======================

**ansible-kerberos-server** is an Ansible role to easily install a Kerberos Server.


Requirements
------------

In order to use this Ansible role, you will need:

* Ansible version >= 2.2 in your deployer machine.
* Check meta/main.yml if you need to check dependencies.


Main workflow
-------------

This role does:
* Download specific Kerberos packages (this packages are os-dependent).
* Configuring Kerberos Server files:
 * kdc.conf
 * kadm5.acl
 * krb5.conf
* Create an admin user

Role Variables
--------------


| Attribute 		| Default Value 	| Description  									|
|---        		|---				|---											|
| realm_name  		| REALM.NAME.COM	| Realm Name for Kerberos Server				|
| kdc_port  		| 88			  	| Kerberos Key Distribution Center (KDC) port 	|
| master_db_pass  	| m4st3r_p4ssw0rd  	| Administrator password					  	|
| kadmin_user  		| defaultuser 	 	| Kadmin username							  	|
| kadmin_pass  		| d3f4ultp4ss  		| Kadmin password							  	|
